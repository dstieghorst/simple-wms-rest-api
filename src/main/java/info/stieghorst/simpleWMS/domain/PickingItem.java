package info.stieghorst.simpleWMS.domain;

import lombok.Data;

/*
 * picking item
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class PickingItem {
	
	public static final String STATUS_OPEN = "OFFEN";
	public static final String STATUS_FINISHED = "ERLEDIGT";
	
	// fields

	private Long id;
	
	private PickingOrder order;
	
	private Product product;
	
	private Integer orderQuantity;
	
	private Integer actualQuantity;
	
	private String itemStatus;
	
	public PickingItem() {
		actualQuantity = 0;
		itemStatus = STATUS_OPEN;
	}

	public PickingItem(PickingOrder order, Product product, Integer orderQuantity) {
		this.order = order;
		this.product = product;
		this.orderQuantity = orderQuantity;
		this.actualQuantity = 0;
		this.itemStatus = STATUS_OPEN;
	}
	
	

}
