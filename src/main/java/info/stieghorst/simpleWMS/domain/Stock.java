package info.stieghorst.simpleWMS.domain;

import lombok.Data;

/*
 * stock: product - location - units
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class Stock {

	// fields

	private Long id;

	private Product product;

	private Location loc;

	private Integer units;

	// constructors
	
	public Stock() {
	}

	public Stock(Product product, Location loc, Integer units) {
		this.product = product;
		this.loc = loc;
		this.units = units;
	}

}
