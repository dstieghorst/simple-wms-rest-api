package info.stieghorst.simpleWMS.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/*
 * picking order
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
@ToString(exclude="items")
public class PickingOrder {
	
	public static final String STATUS_NEW = "NEU";
	public static final String STATUS_IN_PROGRESS = "IN ARBEIT";
	public static final String STATUS_FINISHED = "ERLEDIGT";
	
	// fields

	private Long id;

	private String orderName;
	
	private String orderStatus;
	
	private User user;
	
    private Date created;

    private Date lastChanged;
	
	private List<PickingItem> items;

	public PickingOrder() {
		this.created = new Date();
	}
	
	public PickingOrder(String orderName) {
		this.orderName = orderName;
		this.orderStatus = STATUS_NEW;
		this.created = new Date();
		this.items = new ArrayList<>();
	}
	
	public void addItem(PickingItem item) {
		if(items == null) {
			items = new ArrayList<>();
		}
		items.add(item);
	}
	
	public void changeStatus() {
		if(isStatusChangeAllowed()) {
			if(orderStatus.equals(STATUS_NEW)) {
				orderStatus = STATUS_IN_PROGRESS; 
			}
			else if(orderStatus.equals(STATUS_IN_PROGRESS)) {
				orderStatus = STATUS_FINISHED;
			}
		}
	}
	
	@JsonIgnore
	public boolean isStatusChangeAllowed() {
		if(orderStatus.equals(STATUS_NEW) && items != null && items.size() > 0) {
				return true;
		}
		else if(orderStatus.equals(STATUS_IN_PROGRESS)) {
			boolean allItemsFinished = true;
			for(PickingItem item : items) {
				if(item.getItemStatus().equals(PickingItem.STATUS_OPEN)) {
					allItemsFinished = false;
					break;
				}
			}
			return allItemsFinished;
		}
		
		/* 
		 * in every other case:
		 * - STATUS_NEW and no PickingItem yet
		 * - STATUS_FINISHED
		 * (STATUS_IN_PROGRESS: all cases handled above)
		 *  no status change!
		 */
		return false;
	}
	
	/*
	 * to be able to change to status display,
	 * work with the constants
	 */
	@JsonIgnore
	public boolean isStatusNew() {
		return orderStatus.equals(STATUS_NEW);
	}
	
	@JsonIgnore
	public boolean isAssignmentAllowed() {
		return orderStatus.equals(STATUS_NEW)
				&& user == null;
	}
	
	/*
	 * can this picking order be closed?
	 * conditions: 
	 * - status has to in STATUS_IN_PROGRESS
	 * - no item may be in STATUS_OPEN
	 * 
	 */
	@JsonIgnore
	public boolean isClosingAllowed() {
		if(orderStatus.equals(STATUS_IN_PROGRESS)) {
			boolean allItemsFinished = true;
			for(PickingItem item : items) {
				if(item.getItemStatus().equals(PickingItem.STATUS_OPEN)) {
					allItemsFinished = false;
					break;
				}
			}
			return allItemsFinished;
		}
		else {
			return false;
		}
	}
	
	

}
