package info.stieghorst.simpleWMS.domain;

import lombok.Data;

/*
 * storage location
 * 3 part coordinates
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class Location {

	// fields

	private Long id;

	private Integer aisle;
	
	private Integer stack;
	
	private Integer level;
	
	private Boolean locked;
	
	private Stock stock;

	// constructors
	
	public Location() {
		
	}

	public Location(Integer aisle, Integer stack, Integer level) {
		this.aisle = aisle;
		this.stack = stack;
		this.level = level;
		this.locked = false; // by default not locked
	}
	
	public String getCoordinates() {
		if(aisle == null && stack == null && level == null) {
			return "";
		}
		if(aisle == 0 && stack == 0 && level == 0) {
			return "";
		}
		else {
			return String.format("%03d", aisle)
					+ "-"
					+ String.format("%03d", stack)
					+ "-"
					+ String.format("%03d", level);
		}
	}

}
