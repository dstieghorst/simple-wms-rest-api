package info.stieghorst.simpleWMS.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

/*
 * stock keeping unit
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
@ToString(exclude="stocks")
public class Product {

	// fields

	private Long id;

	private String productNo;
	
	private String name;
	
	private String description;
	
	private boolean active;
	
	private List <Stock> stocks = new ArrayList<>();

	// constructors
	
	public Product() {
		
	}

	public Product(String productNo, String name, String description, boolean active) {
		this.productNo = productNo;
		this.name = name;
		this.description = description;
		this.active = active;
	}
	
	

}
