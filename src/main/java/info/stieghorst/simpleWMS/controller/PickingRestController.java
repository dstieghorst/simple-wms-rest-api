package info.stieghorst.simpleWMS.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;
import lombok.extern.slf4j.Slf4j;

/* 
 * REST controller for all request from the client
 * (mobile app or JS application)
 * 
 * use Spring RestTemplate to connect to the backend
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
@Slf4j
public class PickingRestController {

	@Value("${backend.url}")
    private String backendUrl;

    private final RestTemplate restTemplate = new RestTemplate();
    
    /*
     * GET list of picking orders
     * either complete list or
     * filtered by optional search criteria
     */
    @GetMapping("/orders")
	public List<PickingOrder> getPickingOrders(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "created", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date created,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "assigned", required = false) String assigned,
			@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "itemId", required = false) Long itemId
			) {
		
		log.debug("getPickingOrders - GET /orders");
		
		String url = backendUrl;
		
		List<PickingOrder> orderList = new ArrayList<>();
		// case 1 - filter by id
		if (id != null) { 
			log.debug("search for id {}", id);
			url = url + "/orders?id=" + id.toString();
		}
		// case 2 - filter by creationDate
		else if(created != null) {
			log.debug("search for created {}", created.toString());
			SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
			String createdString = inputFormat.format(created);
			url = url + "/orders?created=" + createdString;
		}
		// case 3 - filter by status
		else if(status != null && status.trim().length() > 0) {
			log.debug("search for status {}", status.trim());
			url = url + "/orders?status=" + status.trim();
		}
		// case 4 - filter by user assignment
		else if(assigned != null && assigned.trim().length() > 0) {
			String assignmentString = assigned.trim().toLowerCase();
			log.debug("search for assigned {}", assignmentString);

			url = url + "/orders?status=" + status.trim();
			
			if(username != null && username.trim().length() > 0) {
				log.debug("username {}", username);
				url = url + "&username=" + username.trim();
			}
		}
		// case 5 - find orders for itemId
		else if(itemId != null) {
			log.debug("search for itemId {}", itemId);
			url = url + "/orders?itemId=" + itemId.toString();
		}
		// case 6 - no filter
		else {
			log.debug("no filter - get all orders");
			url = url + "/orders";
		}

		log.debug("url: {}", url);
		
		ResponseEntity<PickingOrder[]> response = restTemplate.getForEntity(url, PickingOrder[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			PickingOrder[] orders = response.getBody();
			orderList = new ArrayList<>(Arrays.asList(orders));
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		return orderList;
	}
	
	/*
	 * POST a new pikcing order
	 */
    @PostMapping("/orders")
	public PickingOrder newPickingOrder(@RequestBody PickingOrder newPickingOrder) {
		log.debug("newPickingOrder - POST /orders");
		String url = backendUrl + "/orders";
		log.debug("url: {}", url);
		
		PickingOrder savedOrder = null;
		
		ResponseEntity<PickingOrder> response = restTemplate.postForEntity(url, newPickingOrder, PickingOrder.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			savedOrder = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return savedOrder;
	}
	
	/*
	 * GET a single picking order,
	 * identified by ID
	 */
    @GetMapping("/orders/{id}")
	public PickingOrder getPickingOrderById(@PathVariable Long id) {
		log.debug("getPickingOrderById - GET /orders/{}", id);
		String url = backendUrl + "/orders/" + id.toString();
		log.debug("url: {}", url);
		
		PickingOrder order = null;
		
		ResponseEntity<PickingOrder> response = restTemplate.getForEntity(url, PickingOrder.class); 
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			order = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return order;
	}
	
	/*
	 * PUT changes to a single picking order,
	 * identified by ID
	 */
    @PutMapping("/orders/{id}")
	public void updatePickingOrder(@RequestBody PickingOrder updatedOrder, @PathVariable Long id) {
		log.debug("updatePickingOrder - PUT /orders/{}", id);
		String url = backendUrl + "/orders/" + id.toString();
		log.debug("url: {}", url);
		
		try {
			restTemplate.put(url, updatedOrder);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}
	
	/*
	 * DELETE a single picking order,
	 * identified by ID
	 */
    @DeleteMapping("/orders/{id}")
	public void deletePickingOrder(@PathVariable Long id) {
		log.debug("deletePickingOrder - DELETE /orders/{}", id);
		String url = backendUrl + "/orders/" + id.toString();
		log.debug("url: {}", url);
		
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}
	
	/*
	 * GET list of items for a single picking order,
	 * identified by ID
	 */
    @GetMapping("/orders/{id}/items")
	public List<PickingItem> getItemListForOrder(@PathVariable Long id) {
		log.debug("getItemListForOrder - GET /orders/{}/items", id);
		String url = backendUrl + "/orders/" + id.toString() + "/items";
		log.debug("url: {}", url);
		
		List<PickingItem> itemList = new ArrayList<>();
		
		ResponseEntity<PickingItem[]> response = restTemplate.getForEntity(url, PickingItem[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			PickingItem[] items = response.getBody();
			itemList = new ArrayList<>(Arrays.asList(items));
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		return itemList;
	}
	
	/*
	 * POST a new item for a picking order,
	 * identified by ID
	 */
    @PostMapping("/orders/{orderId}/items")
	public PickingItem addItemToOrder(@PathVariable Long orderId, @RequestBody PickingItem newItem) {
		log.debug("addItemToOrder - POST /orders/{}/items", orderId);
		String url = backendUrl + "/orders/" + orderId.toString() + "/items";
		log.debug("url: {}", url);
		
		PickingItem savedItem = null;
		
		ResponseEntity<PickingItem> response = restTemplate.postForEntity(url, newItem, PickingItem.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			savedItem = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return savedItem;
	}

	/*
	 * GET a list of picking items,
	 * either for an order, identified by its id,
	 * or the complete list
	 */
    @GetMapping("/items")
	List<PickingItem> getPickingItems(@RequestParam(value = "orderId", required = false) Long orderId) {
		log.debug("getPickingItems - GET /items");
		
		String url = backendUrl + "/items";
		
		if(orderId != null) {
			log.debug("search for orderId {}", orderId);
			url = url + "?orderId=" + orderId.toString();
		}
		else {
			log.debug("get all PickingItems");
		}
		
		log.debug("url: {}", url);
		
		List<PickingItem> itemList = new ArrayList<>();
		
		ResponseEntity<PickingItem[]> response = restTemplate.getForEntity(url, PickingItem[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			PickingItem[] items = response.getBody();
			itemList = new ArrayList<>(Arrays.asList(items));
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		return itemList;
	}
	
	/* 
	 * POST a new picking item
	 */
    @PostMapping("/items")
	PickingItem newPickingItem(@RequestBody PickingItem newItem) {
		log.debug("newPickingItem + POST /items");
		log.debug("item {}", newItem);
		
		String url = backendUrl + "/items";
		log.debug("url: {}", url);
		
		PickingItem savedItem = null;
		
		ResponseEntity<PickingItem> response = restTemplate.postForEntity(url, newItem, PickingItem.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			savedItem = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return savedItem;
	}
	
	/* 
	 * GET a single picking item,
	 * identified by ID
	 */
    @GetMapping("/items/{id}")
	PickingItem getPickingItemById(@PathVariable Long id) {
		log.debug("getPickingItemById - GET /items/{}", id);
		
		String url = backendUrl + "items/" + id.toString();
		log.debug("url: {}", url);
		
		PickingItem item = null;
		
		ResponseEntity<PickingItem> response = restTemplate.getForEntity(url, PickingItem.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			item = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return item;
	}
	
	/*
	 * PUT changes to an existing picking item,
	 * identified by ID
	 */
    @PutMapping("/items/{id}")
	void updatePickingItem(@RequestBody PickingItem updatedItem, @PathVariable Long id) {
		log.debug("updatePickingItem - PUT /items/{}", id);
		
		String url = backendUrl + "items/" + id.toString();
		log.debug("url: {}", url);
		
		try {
			restTemplate.put(url, updatedItem);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}
	
	/*
	 * DELETE an existing picking item,
	 * idenfied by ID
	 */
    @DeleteMapping("/items/{id}")
	void deletePickingItem(@PathVariable Long id) {
		log.debug("deletePickingItem - DELETE /items/{}", id);
		
		String url = backendUrl + "items/" + id.toString();
		log.debug("url: {}", url);
		
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}

}
