package info.stieghorst.simpleWMS.security;

/*
 * Spring Security with JWT = JSON Web Token
 * copied from example code found in several sources
 */
public class AccountCredentials {

	private String username;

	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}