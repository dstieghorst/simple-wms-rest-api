package info.stieghorst.simpleWMS.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.domain.User;
import lombok.extern.slf4j.Slf4j;

/*
 * Spring Security with JWT = JSON Web Token
 * copied from example code found in several sources
 * - loading users and roles from the backend
 */
@Service
@Slf4j
public class UserDetailServiceImpl implements UserDetailsService {
	
	@Value("${backend.url}")
    private String backendUrl;

    private final RestTemplate restTemplate = new RestTemplate();
    
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.debug("UserDetailServiceImpl.loadUserByUsername - username = {}", username);
		
		String url = backendUrl + "/users?username=" + username;
		log.debug("url: {}", url);
		
		User currentUser = null;
		
		ResponseEntity<User[]> response = restTemplate.getForEntity(url, User[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			User[] users = response.getBody();
			currentUser = users[0];
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
			throw new UsernameNotFoundException(username);
		}
		
		log.debug("user found: {}/{}", currentUser.getUsername(), currentUser.getPassword());

		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>(currentUser.getRoles().size());
		for(Role role : currentUser.getRoles()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
		}

        UserDetails user 
        	= new org.springframework.security.core.userdetails.User(
        			username, currentUser.getPassword(), 
        			true, true, true, true,
        			authorities);
        
        return user;
	}

}
